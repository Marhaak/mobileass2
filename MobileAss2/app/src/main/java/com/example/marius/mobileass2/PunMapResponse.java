package com.example.marius.mobileass2;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by stensby on 24/09/14.
 * For async tasks to run functions in PunMapActivity.
 */
public interface PunMapResponse {
    public void plotPun(PunWrapper pun);                                    // PunGet -> Activity
    public void setLoadedSquares(HashMap<LatLng, Boolean> loadedSquares);   // PunGetArea ->Activity
}
