package com.example.marius.mobileass2;

import android.os.AsyncTask;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stensby on 24/09/14.
 * Gets puns withing an area from the web server.
 * Returns to PunMapActivity using PunMapResponse (delegate).
 */
public class PunGet extends AsyncTask<LatLng, String, String> {
    public PunMapResponse delegate = null;
    private ArrayList<PunWrapper> wrappedPuns;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        wrappedPuns = new ArrayList<PunWrapper>();
    }

    /**
     * Loads puns, and puts them in the ArrayList wrappedPuns
     * @param ltln LatLng array. Includes the two corners of the area to be downloaded.
     * @return does nothing.
     */

    @Override
    public String doInBackground(LatLng... ltln) {

        // Build request to be sent to server.
        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(new BasicNameValuePair("lat1", String.valueOf(ltln[0].latitude)));
        parameters.add(new BasicNameValuePair("lon1", String.valueOf(ltln[0].longitude)));
        parameters.add(new BasicNameValuePair("lat2", String.valueOf(ltln[1].latitude)));
        parameters.add(new BasicNameValuePair("lon2", String.valueOf(ltln[1].longitude)));

        // Request data from server.
        JSONParser parser = new JSONParser();
        JSONObject jObj = parser.makeHttpRequest("http://www.stud.hig.no/~101561/androidAss2/get_users_in_area.php", "GET", parameters);

        // Try to load puns within the given area.
        try {
            JSONArray puns = jObj.getJSONArray("puns");

            for (int i = 0; i < puns.length(); i++) {
                JSONObject c = puns.getJSONObject(i);

                String title = c.getString("title");
                String author = c.getString("author");
                String thepun = c.getString("thePun");
                LatLng punPos = new LatLng(c.getDouble("lat"), c.getDouble("lon"));
                int id = c.getInt("id");
                PunWrapper pun = new PunWrapper(author, title, thepun, id, punPos, "");
                wrappedPuns.add(pun);
            }

        } catch (JSONException e) {
            //e.printStackTrace();
        }

        return "";
    }

    /**
     * Runs plotPun on the PunMapActivity (technically on the PunMapResponse), for each pun in wrappedPuns.
     * @param s Irrelevant
     */

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        for (PunWrapper pun : wrappedPuns) {
            delegate.plotPun(pun);
        }
    }
}