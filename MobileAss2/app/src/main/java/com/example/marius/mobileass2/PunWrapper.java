package com.example.marius.mobileass2;

import com.google.android.gms.maps.model.LatLng;
import java.util.Date;

/**
 * Created by stensby on 24/09/14.
 * Holds information about a pun.
 */
public class PunWrapper {
    private String author;
    private String title;
    private String pun;
    private Date   date;
    private int id;
    private LatLng pos;
    private String about;

    public PunWrapper(String author, String title, String pun, int id, LatLng pos, String about) {
        this.author = author;
        this.title  = title;
        this.pun    = pun;
        this.date   = new Date();
        this.id = id;
        this.pos = pos;
        this.about = about;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPun() {
        return pun;
    }

    public void setPun(String pun) {
        this.pun = pun;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LatLng getPos() {
        return pos;
    }

    public void setPos(LatLng pos) {
        this.pos = pos;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
