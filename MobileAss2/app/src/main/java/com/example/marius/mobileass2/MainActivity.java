package com.example.marius.mobileass2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // No internet detected.
        if (!Status.getInstance(this).isOnline()) {

            Toast.makeText(getApplicationContext(), R.string.error_internet, Toast.LENGTH_LONG).show();

        }

        // If there is no location activated on the phone
        if (!Status.getInstance(this).haveGps()) {

            // Create an allert dialog for the user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.error_gps));
            dialog.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    // Starting the location settings.
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                   // Nothing.
                }
            });
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings) {

            // Starting the location settings.
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
            return true;
        }
        return true;
    }

    // Invoked by the "See other puns" button
    public void seePuns(View view) {
        startActivity(new Intent(this, PunMapActivity.class));

    }

    // Invoked by the "Make a new pun" button
    public void makePun(View view) {
        Intent intent = new Intent(this, CreateNewPun.class);
        startActivity(intent);
    }

    // Invoked by the "View Puns" button
    public void viewPuns(View view) {
        Intent intent = new Intent(this, DisplayPuns.class);
        startActivity(intent);
    }
}