/**
 * Most of the async class LoadAllPuns is from the link below, we have changed it to fit our solution.
 * http://www.androidhive.info/2012/05/how-to-connect-android-with-php-mysql/
 */

package com.example.marius.mobileass2;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class handel's showing all the puns in the database
 * in a list that the user can scroll thorough.
 */
public class DisplayPuns extends ListActivity {

    private ProgressDialog pDialog;

    private JSONParser jParser = new JSONParser();

    private ArrayList<HashMap<String, String>> punlist;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PUNS = "puns";
    private static final String TAG_TITLE = "title";
    private static final String TAG_AUTHOR = "author";
    private static final String TAG_THEPUN = "thePun";
    private static final String TAG_ABOUT = "about";
    private static final String TAG_DATE = "date";

    private JSONArray puns = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_puns);

        //Get the list view from XML, activity_display_puns
        ListView list = getListView();

        punlist = new ArrayList<HashMap<String, String>>();

        new LoadAllPuns().execute();

        getActionBar().setIcon(R.drawable.ic_list);

        // Setting click listener to the ListView.
        // The view starts with only title and author visible. When clicked, it should display
        // the pun, about, if there is something is about, and date.
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Get the textviews in list_item.
                TextView thePunView = (TextView) view.findViewById(R.id.thePunDisplay);
                TextView aboutView = (TextView) view.findViewById(R.id.aboutDisplay);
                TextView dateView = (TextView) view.findViewById(R.id.dateDisplay);
                TextView aboutTitle = (TextView) view.findViewById(R.id.aboutTitle);

                // When an item in the list is clicked, check if the items are visible.
                if(thePunView.getVisibility() == View.VISIBLE) {
                    thePunView.setVisibility(View.GONE);
                    aboutTitle.setVisibility(View.GONE);
                    aboutView.setVisibility(View.GONE);
                    dateView.setVisibility(View.GONE);
                } else {
                    // If the items are not visible, check if the about tag is empty
                    if(((TextView) view.findViewById(R.id.aboutDisplay)).getText().toString().equals("")) {
                        // If there's nothing in the about tag, only set the pun and date visible.
                        // Else display everything.
                        thePunView.setVisibility(View.VISIBLE);
                        dateView.setVisibility(View.VISIBLE);
                    } else {
                        thePunView.setVisibility(View.VISIBLE);
                        aboutTitle.setVisibility(View.VISIBLE);
                        aboutView.setVisibility(View.VISIBLE);
                        dateView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

    }

    /**
     * This class pulls all the puns from the database and store them in a ArrayList
     */
    private class LoadAllPuns extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DisplayPuns.this);
            pDialog.setMessage(String.valueOf(R.string.loading_puns));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // Making the http request.
            String url_puns = "http://www.stud.hig.no/~101561/androidAss2/list_all_puns.php";
            JSONObject json = jParser.makeHttpRequest(url_puns, "GET", params);

            try {

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    puns = json.getJSONArray(TAG_PUNS);

                    // Looping through the answer you got from the php file.
                    for (int i = 0; i < puns.length(); i++) {
                        JSONObject c = puns.getJSONObject(i);

                        String title  = c.getString(TAG_TITLE);
                        String author = " - ";
                        author       += c.getString(TAG_AUTHOR);
                        String thepun = c.getString(TAG_THEPUN);
                        String about  = c.getString(TAG_ABOUT);
                        String date   = c.getString(TAG_DATE);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(TAG_TITLE, title);
                        map.put(TAG_AUTHOR, author);
                        map.put(TAG_THEPUN, thepun);
                        map.put(TAG_ABOUT, about);
                        map.put(TAG_DATE, date);

                        punlist.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all puns
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            DisplayPuns.this, punlist,
                            R.layout.list_item, new String[] { TAG_TITLE,
                            TAG_AUTHOR, TAG_THEPUN, TAG_ABOUT, TAG_DATE},
                            new int[] { R.id.titleDisplay, R.id.authorDisplay, R.id.thePunDisplay,
                                    R.id.aboutDisplay, R.id.dateDisplay });
                    // updating the Listview
                    setListAdapter(adapter);
                }
            });

        }
    }
}
