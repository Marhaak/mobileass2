/**
 * Most of the gps related code here is from the link below
 * https://developer.android.com/training/location/retrieve-current.html and further along the tutorial
 *
 * The Async class PostNewPun is from the link below, we have modified it to fit your solution.
 * http://www.androidhive.info/2012/05/how-to-connect-android-with-php-mysql/
 */

package com.example.marius.mobileass2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class handel's the making of a new pun, it uses gps to get the lat/long coordinates
 * and sends it to the database.
 */
public class CreateNewPun extends Activity implements LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private EditText getTitle;
    private EditText getAuthor;
    private EditText getPun;
    private EditText getAbout;
    private Spinner getSpinner;
    private ProgressDialog pDialog;

    private JSONParser jsonParser = new JSONParser();

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    // A request to connect to Location Services
    private LocationRequest mLocationRequest;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;


    // Handle to SharedPreferences for this app
    private SharedPreferences mPrefs;

    // Handle to a SharedPreferences editor
    private SharedPreferences.Editor mEditor;

    /*
    * Note if updates have been turned on. Starts out as "false"; is set to "true" in the
    * method handleRequestSuccess of LocationUpdateReceiver.
    *
    */
    private boolean mUpdatesRequested = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_pun);

        // Populate the spinner
        getSpinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.languages, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getSpinner.setAdapter(adapter);

        // Handlers to the ui elements
        getTitle = (EditText)findViewById(R.id.titleText);
        getAuthor = (EditText)findViewById(R.id.authorText);
        getPun = (EditText)findViewById(R.id.thePunText);
        getAbout = (EditText)findViewById(R.id.aboutText);

        // Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

         //Set the update interval
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Note that location updates are off until the user turns them on
        mUpdatesRequested = false;

        // Open Shared Preferences
        mPrefs = getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get an editor
        mEditor = mPrefs.edit();

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        mLocationClient = new LocationClient(this, this, this);

        // Adds a icon to the action Bar
        try {
            getActionBar().setIcon(R.drawable.ic_upload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Hiding the keyboard.
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /*
   * Called when the Activity is no longer visible at all.
   * Stop updates and disconnect.
   */
    @Override
    public void onStop() {

        // If the client is connected
        if (mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // After disconnect() is called, the client is considered "dead".
        mLocationClient.disconnect();

        super.onStop();
    }

    /*
    * Called when the Activity is going into the background.
    * Parts of the UI may be visible, but the Activity is inactive.
    */
    @Override
    public void onPause() {

        // Save the current setting for updates
        mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, mUpdatesRequested);
        mEditor.commit();

        // Pause the mother class
        super.onPause();
    }

    /*
     * Called when the Activity is restarted, even before it becomes visible.
     */
    @Override
    public void onStart() {

        // Start the mother class
        super.onStart();

        /*
         * Connect the client. Don't re-start any requests here;
         * instead, wait for onResume()
         */
        mLocationClient.connect();
    }

    /*
    * Called when the system detects that this Activity is now visible.
    */
    @Override
    public void onResume() {
        super.onResume();

        // If the app already has a setting for getting location updates, get it
        if (mPrefs.contains(LocationUtils.KEY_UPDATES_REQUESTED)) {
            mUpdatesRequested = mPrefs.getBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);

            // Otherwise, turn off location updates until requested
        } else {
            mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);
            mEditor.commit();
        }

    }

    /*
    * Handle results returned to this Activity by other Activities started with
    * startActivityForResult(). In particular, the method onConnectionFailed() in
    * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
    * start an Activity that handles Google Play services problems. The result of this
    * call returns here, to onActivityResult.
    */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        // Choose what to do based on the request code
        switch (requestCode) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:

                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.resolved));

                        // Display the result
                        //mConnectionState.setText(R.string.connected);
                        //mConnectionStatus.setText(R.string.resolved);
                        break;

                    // If any other result was returned by Google Play services
                    default:
                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.no_resolution));

                        // Display the result
                        //mConnectionState.setText(R.string.disconnected);
                        //mConnectionStatus.setText(R.string.no_resolution);

                        break;
                }
                break;

            // If any other request code was received
            default:
                // Report that this Activity received an unknown requestCode
                Log.d(LocationUtils.APPTAG,
                        getString(R.string.unknown_activity_request_code, requestCode));

                break;
        }
    }

    /**
     * Called when the loaction changes
     * @param location Holds the information about the current location.
     */
    @Override
    public void onLocationChanged(Location location) { }

    /**
    * Called by Location Services when the request to connect the
    * client finishes successfully. At this point, you can
    * request the current location or start periodic updates
    */
    @Override
    public void onConnected(Bundle bundle) {

        if (mUpdatesRequested) {
            startPeriodicUpdates();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {
            // Show a error message with a toast.
            Toast.makeText(getApplicationContext(), R.string.error_gps, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDisconnected() {
        //mConnectionStatus.setText(R.string.disconnected);
    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
        //mConnectionState.setText(R.string.location_updates_stopped);
    }

    // Revoked by the "Submit Pun" button.
    public void submitPun (View view) {

        new PostNewPun().execute();
    }

    // A async class to sending the new created pun to the database.
    private class PostNewPun extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(CreateNewPun.this);
            pDialog.setMessage(String.valueOf(R.string.creating_pun));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            // Getting the values of the textfields.
            String title = getTitle.getText().toString();
            String author = getAuthor.getText().toString();
            String pun = getPun.getText().toString();
            String about = getAbout.getText().toString();
            String spinner = getSpinner.getSelectedItem().toString();
            String language;

            // Setting the right language tag.
            if(spinner.equalsIgnoreCase(getString(R.string.english))) {

                language = "EN";
            } else {

                language = "NO";
            }
            // Get the current location
            Location currentLocation = mLocationClient.getLastLocation();
            List<NameValuePair> parameters = new ArrayList<NameValuePair>();

            // Setting up the parameters for the add_pun.php script
            parameters.add(new BasicNameValuePair("title", title));
            parameters.add(new BasicNameValuePair("author", author));
            parameters.add(new BasicNameValuePair("thePun", pun));
            parameters.add(new BasicNameValuePair("about", about));
            parameters.add(new BasicNameValuePair("language", language));

            // If there is no location found set lat/long to a random number.
            if(currentLocation == null) {
                double minLat = -90.0d;
                double maxLat = 90.0d;
                double minLon = -180.0d;
                double maxLon = 180.0d;

                Random random = new Random();
                NumberFormat formater = NumberFormat.getInstance();
                formater.setMinimumFractionDigits(7);
                formater.setMaximumFractionDigits(7);

                // Random Latitude
                String lat = formater.format(minLat + (maxLat - minLat) * random.nextDouble());
                // Random Longitude
                String lon = formater.format(minLon + (maxLon - minLon) * random.nextDouble());

                parameters.add(new BasicNameValuePair("lat", lat));
                parameters.add(new BasicNameValuePair("lon", lon));
            } else {
                parameters.add(new BasicNameValuePair("lat", String.valueOf(currentLocation.getLatitude())));
                parameters.add(new BasicNameValuePair("lon", String.valueOf(currentLocation.getLongitude())));
            }


            String url = "http://www.stud.hig.no/~101561/androidAss2/add_pun.php";
            JSONObject json = jsonParser.makeHttpRequest(url, "POST", parameters);

            Log.d("Json", json.toString());
            Log.d("Create Response", json.toString());

            //Check for success
            try {
                int success = json.getInt(TAG_SUCCESS);

                //Everything went ok
                if(success == 1){

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            } catch(JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {

            pDialog.cancel();
        }
    }
}
