package com.example.marius.mobileass2;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.HashMap;

/**
 * Shows a map with markers. When a marker is tapped by the user it will show a pun.
 * Loads puns from "squares" (each degree of latitude an longitude) whenever you move to see a "square"
 * that has not yet been loaded.
 */

public class PunMapActivity extends FragmentActivity implements PunMapResponse {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private HashMap<LatLng, Boolean> loadedSquares;
    private final PunMapResponse me = this; // This is used when the camera is moved.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pun_map);
        setUpMapIfNeeded();
        loadedSquares  = new HashMap<LatLng, Boolean>();

        getActionBar().setIcon(R.drawable.ic_map);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("See where all the puns are");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        // Enable MyLocation Layer of Google Map
        mMap.setMyLocationEnabled(true);

        // Set the map listener
        setMapListener();

        // Get LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(provider);

        LatLng latLng;

        if (myLocation == null) {
            // The users location is not currently available
            latLng = new LatLng(60.790172, 10.683130);
            Log.e("Position", "Unable to get the users position");
        } else {

            // Get latitude of the current location
            double latitude = myLocation.getLatitude();

            // Get longitude of the current location
            double longitude = myLocation.getLongitude();

            // Create a LatLng object for the current location
            latLng = new LatLng(latitude, longitude);
        }

        // Show the current location in Google Map
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
    }

    /**
     * Sets the onCameraChangeListener.
     */
    public void setMapListener() {
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            /**
             * Loads puns within the borders of the camera when it is moved.
             */
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                LatLngBounds curScreen = mMap.getProjection().getVisibleRegion().latLngBounds;

                PunGetArea punGetArea = new PunGetArea();
                punGetArea.delegate = me; // Unable to use "this", as we are technically in an OnCameraChangeListener object.
                punGetArea.loadedSquares = loadedSquares;
                punGetArea.execute(curScreen.northeast, curScreen.southwest);
            }
        });
    }





    /**
     * Implemented from interface PunMapResponse.
     *
     * puts a marker on the map, that will reveal the pun when clicked.
     * @param pun A pun to plot on the map.
     */
    public void plotPun(PunWrapper pun) {

        MarkerOptions options = new MarkerOptions();

        options.position(pun.getPos());
        options.title(pun.getTitle() + " by " + pun.getAuthor());
        options.snippet(pun.getPun());

        mMap.addMarker(options);
    }

    /**
     * Implemented from interface PunMapResponse.
     * @param loadedSquares The loadedSquares to replace the current one.
     */

    @Override
    public void setLoadedSquares(HashMap<LatLng, Boolean> loadedSquares) {
        this.loadedSquares = loadedSquares;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }
}
