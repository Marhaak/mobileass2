package com.example.marius.mobileass2;

import android.os.AsyncTask;
import com.google.android.gms.maps.model.LatLng;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

/**
 * Created by stensby on 30/09/14.
 *
 * loads puns if they are not yet loaded.
 */
public class PunGetArea extends AsyncTask<LatLng, String, String> {
    public HashMap<LatLng, Boolean> loadedSquares;
    PunMapResponse delegate = null;

    @Override
    protected String doInBackground(LatLng... params) {
        loadUnloadedPunsInArea(params[0], params[1]);
        return null;
    }

    /**
     * Loads unloaded puns within a certain area.
     * @param NE The north east corner of the area to load from
     * @param SW The south west --||--
     */

    private void loadUnloadedPunsInArea(LatLng NE, LatLng SW) {
        // Run trough the plots to load data from.
        for (double latitude = round(SW.latitude, 0, RoundingMode.DOWN); latitude < NE.latitude; latitude+=1d) {
            for (double longitude = round(SW.longitude, 0, RoundingMode.DOWN); longitude < NE.longitude; longitude+=1d) {
                latitude = round(latitude, 0, RoundingMode.HALF_DOWN);   // Those darn Doubles won't stay put!
                longitude = round(longitude, 0, RoundingMode.HALF_DOWN);

                LatLng southwest = new LatLng(latitude, longitude);

                // if the current area has not been loaded previously.
                if (!loadedSquares.containsKey(southwest)) {
                    LatLng northeast = new LatLng(latitude+1d, longitude + 1d);
                    loadedSquares.put(southwest, true);
                    PunGet punGetter = new PunGet();
                    punGetter.delegate = delegate; // Make punGetter return data to PunMapActivity
                    punGetter.execute(northeast, southwest);
                }
            }
        }
        delegate.setLoadedSquares(loadedSquares);
    }
    /**
     * Rounds a double.
     *
     * From http://stackoverflow.com/a/2808648
     * Needs to be used often, as doubles can be tricky to work with.
     *
     * @param value the value to round off.
     * @param places To how many places the value will be rounded past the decimal point.
     * @param rm What mode to round the value with.
     */
    private static double round(double value, int places, RoundingMode rm) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, rm);
        return bd.doubleValue();
    }
}