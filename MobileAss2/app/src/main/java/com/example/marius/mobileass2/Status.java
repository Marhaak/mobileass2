package com.example.marius.mobileass2;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


/**
 * Created by Marius on 30.09.2014.
 * This class contains some methods for the status of the phone
 * Contains a method for internet and location.
 */
public class Status {

    private static Status instance = new Status();
    private static Context context;
    private ConnectivityManager connectivityManager;
    private boolean connected = false;

    public static Status getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    /**
     *          Tests if the phone has internet
     * @return  True if internet, false if not internet
     */
    public boolean isOnline() {
        try {

            // Testing if there is any internet.
            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;

        } catch (Exception e) {

            Log.d("Connectivity", e.toString());
        }
        return connected;
    }

    /**
     *          Tests if there is any Location service active.
     * @return  True if active, false if not.
     */
    public boolean haveGps() {

        LocationManager locationManager;
        boolean gps_enabled;
        boolean network_enabled;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        return gps_enabled && network_enabled;
    }
}
