<?php
 
/*
 * Following code will get single product details
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["lat1"]) && isset($_GET["lon1"]) && isset($_GET["lat2"]) && isset($_GET["lon2"])) {

    $latitude1 = $_GET['lat1'];
	$longitude1 = $_GET['lon1'];
	$latitude2 = $_GET['lat2'];
	$longitude2 = $_GET['lon2'];
	
 
    // get a product from products table
	if($latitude1 < $latitude2) {
	
		if($longitude1 < $longitude2) {
			$result = mysql_query("SELECT * FROM pun WHERE(lat BETWEEN '$latitude1' AND '$latitude2')AND(lon BETWEEN '$longitude1' AND '$longitude2')");
		} else {
			$result = mysql_query("SELECT * FROM pun WHERE(lat BETWEEN '$latitude1' AND '$latitude2')AND(lon BETWEEN '$longitude2' AND '$longitude1')");
		}
	} else {
		if($longitude1 < $longitude2) {
			$result = mysql_query("SELECT * FROM pun WHERE(lat BETWEEN '$latitude2' AND '$latitude1')AND(lon BETWEEN '$longitude1' AND '$longitude2')");
		} else {
			$result = mysql_query("SELECT * FROM pun WHERE(lat BETWEEN '$latitude2' AND '$latitude1')AND(lon BETWEEN '$longitude2' AND '$longitude1')");
		}
	}
 
    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {
 
           $response["puns"] = array();
 	    while($row = mysql_fetch_array($result)) {
            
		$pun = array();
		$pun["id"] = $row["id"];
            	$pun["title"] = $row["title"];
            	$pun["author"] = $row["author"];
            	$pun["thePun"] = $row["thePun"];
	    	$pun["about"] = $row["about"];
	    	$pun["date"] = $row["date"];
	    	$pun["language"] = $row["language"];
	    	$pun["lat"] = $row["lat"];
	    	$pun["lon"] = $row["lon"];

 	       	array_push($response["puns"], $pun);
	    }
            // success
            $response["success"] = 1;
 
            // user node
            //$response["pun"] = array();
 
           
 
            // echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No puns in area found";
 
            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No puns is punable";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>