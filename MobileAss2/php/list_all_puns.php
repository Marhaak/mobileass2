<?php

 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_config.php';

$host = DB_SERVER;
$db = DB_DATABASE;

// connecting to db
$con = new PDO("mysql:host=$host;dbname=$db", DB_USER, DB_PASSWORD) or die(mysql_error());

$result = $con->query("SELECT * FROM pun ORDER BY date DESC");
$result->setFetchMode(PDO::FETCH_ASSOC);

$response["puns"] = array();

while($row = $result->fetch()) {
	
	$pun = array();
	$pun["title"] = $row["title"];
	$pun["author"] = $row["author"];
	$pun["thePun"] = $row["thePun"];
	$pun["about"] = $row["about"];
	$pun["date"] = $row["date"];
	$pun["language"] = $row["language"];
	$pun["lat"] = $row["lat"];
	$pun["lon"] = $row["lon"];

	array_push($response["puns"], $pun);

	
}
// success
$response["success"] = 1;

// echoing JSON response
echo json_encode($response);

?>