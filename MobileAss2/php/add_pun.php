<?php
// array for JSON response
$response = array();
 
// check for required fields

if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['thePun']) && isset($_POST['about'])
	 && isset($_POST['language']) && isset($_POST['lat']) && isset($_POST['lon'])) {
	
 	$Title = $_POST['title'];
 	$Author = $_POST['author'];
	$ThePun = $_POST['thePun'];
	$About = $_POST['about'];
	$Language = $_POST['language'];
	$Lat = $_POST['lat'];
	$Lon = $_POST['lon'];

    require_once __DIR__ . '/db_connect.php';
	
    // connecting to db
    $db = new DB_CONNECT();
	
    // mysql inserting a new row
    $result = mysql_query("INSERT INTO pun(title, author, thePun, about, language, lat, lon)VALUES('$Title', '$Author', '$ThePun', '$About', '$Language', '$Lat', '$Lon')");
	
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Latitude and longetude successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>